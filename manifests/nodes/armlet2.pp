node armlet2 {
# Location: Scaleway (Iliad/Online datacenter)
#
    include common::default_mageia_server
    include mga_buildsystem::buildnode
    timezone::timezone { 'Europe/Paris': }
}
