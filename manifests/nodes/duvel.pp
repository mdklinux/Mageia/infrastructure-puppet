node duvel {
# Location: IELO datacenter (marseille)
#
# TODO:
# - GIT server
# - setup maintainers database (with web interface)
#
    include common::default_mageia_server
    timezone::timezone { 'Europe/Paris': }
    include main_mirror
    include openldap::master
    include git::client
    include subversion::client
    include subversion::server
    include puppet::master
    include reports::ii

    include sshkeys::keymaster
    include mga_buildsystem::mainnode
    include softwarekey
    include mgasoft

    include access_classes::committers
    include restrictshell::allow_git
    include restrictshell::allow_svn
    include restrictshell::allow_pkgsubmit
    include restrictshell::allow_maintdb
    include restrictshell::allow_upload_bin
    include openssh::ssh_keys_from_ldap

    include repositories::subversion

    # include irkerd

    include websites::svn
    include websites::git

    class { 'mga-advisories':
        vhost => "advisories.${::domain}",
    }

    git::snapshot { '/etc/puppet':
        source => "git://git.${::domain}/infrastructure/puppet/"
    }

    mirror_cleaner::orphans {  'cauldron':
        base => '/distrib/bootstrap/distrib/',
    }

    class { 'mgagit':
        ldap_server => "ldap.${::domain}",
        binddn      => 'cn=mgagit-valstar,ou=System Accounts,dc=mageia,dc=org',
        bindpw      => extlookup('mgagit_ldap','x'),
    }
}
