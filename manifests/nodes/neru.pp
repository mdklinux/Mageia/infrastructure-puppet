node neru {
# Location: Scaleway Paris
#
    include common::default_mageia_server_no_smtp
    timezone::timezone { 'Europe/Paris': }
    include postfix::server::secondary
    include blog::base
    include blog::db_backup
    include blog::files_bots
    include blog::files_backup
    include mysql::server
    include dns::server

    include planet
    include websites::archives
    include websites::static
    include websites::hugs
    include websites::releases
    include websites::www
    include websites::doc
    include websites::start
    include websites::meetbot
    include dashboard
    include access_classes::web
    include openssh::ssh_keys_from_ldap

    openldap::slave_instance { '1':
        rid => 1,
    }

    # http server for meetbot logs
    include apache::base
}
# Other services running on this server :
# - meetbot
