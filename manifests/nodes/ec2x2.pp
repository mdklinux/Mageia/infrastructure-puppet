node ec2x2 {
# Location: Amazon (eu-central-1a)
#
    include common::default_mageia_server
    include mga_buildsystem::buildnode
    timezone::timezone { 'Europe/Paris': }
}
