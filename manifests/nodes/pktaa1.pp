node pktaa1 {
# Location: Equinix Metal / SV - SJC1
#
    include common::default_mageia_server
    include mga_buildsystem::buildnode
    timezone::timezone { 'Europe/Paris': }
}
