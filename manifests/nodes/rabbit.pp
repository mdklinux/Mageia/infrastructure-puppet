node rabbit {
# Location: IELO datacenter (marseille)
#
# - used to create isos ( and live, and so on )
#
    include common::default_mageia_server
    timezone::timezone { 'Europe/Paris': }
    include bcd::base
    #include bcd::web
    include bcd::rsync
    include mga_buildsystem::buildnode
    include draklive
    include git::svn
    include access_classes::iso_makers
    include openssh::ssh_keys_from_ldap
    # include mirror::mageia
    include releasekey

    youri-check::config {'config_cauldron':
        version => 'cauldron',
    }
    youri-check::check {'check_cauldron':
        version => 'cauldron',
        hour    => '*/2',
        minute  => 10
    }

    youri-check::config {'config_6':
        version => '6',
    }
    youri-check::check {'check_6':
        version => '6',
        hour    => '*/4',
        minute  => 30
    }

    youri-check::config {'config_5':
        version => '5',
    }
    youri-check::check {'check_5':
        version => '5',
        hour    => '*/6',
        minute  => 30
    }

    # for testing iso quickly
    # include libvirtd::kvm
    # libvirtd::group_access { 'mga-iso_makers': }

}
