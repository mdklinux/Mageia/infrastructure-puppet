# server for various task
node sucuk {
# Location: IELO datacenter (marseille)
    include common::default_mageia_server_no_smtp
    timezone::timezone { 'Europe/Paris': }

    include access_classes::admin

    include postgresql::server
    postgresql::tagged { 'default': }

    class {'epoll::var':
        db_password => extlookup('epoll_pgsql','x'),
        password => extlookup('epoll_password','x'),
    }
    
    #include epoll
    #include epoll::create_db

    include sympa::server
    include postfix::server::primary
    include lists

    include catdap
    include mga-mirrors

    include wikis
    include websites::perl
    include websites::www
    include websites::nav

    include bugzilla

    # gitweb
    include repositories::git_mirror
    include cgit
    include gitmirror

    include repositories::svn_mirror
    include viewvc

#    include mirrorbrain

    include dns::server

    include xymon::server
    apache::vhost_simple { "xymon.${::domain}":
        location => '/usr/share/xymon/www',
    }

    class { 'mgapeople':
        ldap_server => "ldap.${::domain}",
        binddn      => 'cn=mgapeople-alamut,ou=System Accounts,dc=mageia,dc=org',
        bindpw      => extlookup('mgapeople_ldap','x'),
        vhost       => "people.${::domain}",
        vhostdir    => "/var/www/vhosts/people.${::domain}",
        maintdburl  => "http://pkgsubmit.${::domain}/data/maintdb.txt",
    }

    class { 'mga-treasurer':
        vhost    => "treasurer.${::domain}",
        vhostdir => "/var/www/vhosts/treasurer.${::domain}",
    }

    youri-check::report_www { 'check': }

    youri-check::createdb_user {'config_cauldron':
        version => 'cauldron',
    }

    youri-check::config {'config_cauldron':
        version => 'cauldron',
    }
    youri-check::report { 'report_cauldron':
        version => 'cauldron',
        hour    => '*/2',
        minute  => '0'
    }

    youri-check::createdb_user {'config_9':
        version => '9',
    }

    youri-check::config {'config_9':
        version => '9',
    }

    youri-check::report {'report_9':
        version => '9',
        hour    => '*/4',
        minute  => '56'
    }

    youri-check::createdb_user {'config_8':
        version => '8',
    }

    youri-check::config {'config_8':
        version => '8',
    }

    youri-check::report {'report_8':
        version => '8',
        hour    => '*/4',
        minute  => '56'
    }

    include tld_redirections

    # temporary, just the time the vm is running there
    host { 'friteuse':
        ensure       => 'present',
        ip           => '192.168.122.131',
        host_aliases => [ "friteuse.${::domain}", "forums.${::domain}" ],
    }

    # to create all phpbb database on sucuk
    phpbb::databases { $fqdn: }

    apache::vhost::redirect_ssl { "forums.${::domain}": }
    apache::vhost_redirect { "forum.${::domain}":
        url => "https://forums.${::domain}/",
    }
    apache::vhost_redirect { "ssl_forum.${::domain}":
        url     => "https://forums.${::domain}/",
        vhost   => "forum.${::domain}",
        use_ssl => true,
    }

    # connect to ssl so the proxy do not shoke if trying to
    # enforce ssl ( note that this has not been tested, maybe this
    # is uneeded )
    apache::vhost::reverse_proxy { "ssl_forums.${::domain}":
        url     => "https://forums.${::domain}/",
        vhost   => "forums.${::domain}",
        use_ssl => true,
        content => '
        RewriteEngine On
        RewriteCond %{QUERY_STRING} mode=register
        RewriteRule .*ucp.php - [forbidden]
        ',
    }

    include libvirtd::kvm

}
