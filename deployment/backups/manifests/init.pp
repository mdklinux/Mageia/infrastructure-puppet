class backups {
    class server {

        $backups_dir = '/data/backups'
        $confdir = "${backups_dir}/conf"

        class { 'rsnapshot::base':
            confdir => $confdir,
        }

        file { $backups_dir:
            ensure => directory,
            owner  => root,
            group  => root,
            mode   => '0700',
        }

        rsnapshot::backup{ 'neru':
            snapshot_root => "${backups_dir}/neru",
            backup        => [ "root@neru.${::domain}:/home/irc_bots/meetings meetbot" ],
        }
    }
}
