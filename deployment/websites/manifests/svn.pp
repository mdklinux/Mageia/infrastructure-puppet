class websites::svn {
    apache::vhost_redirect { "svn.${::domain}":
        url => "https://svnweb.${::domain}/",
    }
}
