class websites::doc {
    include websites::base
    $vhost = "doc.${::domain}"
    $vhostdir = "${websites::base::webdatadir}/${vhost}"
    $git_location = "git://git.${::domain}/web/doc"

    apache::vhost::base { $vhost:
        location => $vhostdir,
    }

    apache::vhost::base { "ssl_${vhost}":
        vhost    => $vhost,
        use_ssl  => true,
        location => $vhostdir,
    }

    git::snapshot { $vhostdir:
        source => $git_location,
    }
}
