class websites::hugs {
    include websites::base

    $vhostdir = "${websites::base::webdatadir}/hugs.${::domain}"
    $git_location = "git://git.${::domain}/web/hugs"

    apache::vhost::base { "hugs.${::domain}":
        location => $vhostdir,
    }

    git::snapshot { $vhostdir:
        source => $git_location
    }

    package { 'php-exif': }
}
