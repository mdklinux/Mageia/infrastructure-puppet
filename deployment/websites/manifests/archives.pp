class websites::archives {
    include websites::base
    $vhost = "archives.${::domain}"
    $vhostdir = "${websites::base::webdatadir}/${vhost}"
    $git_location = "git://git.${::domain}/web/archives"

    apache::vhost::base { $vhost:
        location => $vhostdir,
    }

    apache::vhost::base { "ssl_${vhost}":
        vhost    => $vhost,
        use_ssl  => true,
        location => $vhostdir,
    }

    git::snapshot { $vhostdir:
        source => $git_location,
    }
}
