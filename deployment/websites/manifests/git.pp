class websites::git {
    apache::vhost_redirect { "git.${::domain}":
        url => "https://gitweb.${::domain}/",
    }
}
