class websites::start {
    include websites::base
    apache::vhost_redirect { "start.${::domain}":
        url => "https://www.${::domain}/community/",
    }
}
