# We should rather have a meetbot module used to deploy
# it, setup backups and this website
class websites::meetbot {
    $vhost = "meetbot.${::domain}"
    $vhostdir = "/home/irc_bots/meetings/"

    apache::vhost::other_app { "meetbot.${::domain}":
        vhost_file => 'websites/vhost_meetbot.conf',
    }

    file { $vhostdir:
        ensure => directory,
    }
}
