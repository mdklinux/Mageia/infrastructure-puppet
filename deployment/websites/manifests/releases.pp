class websites::releases {
    include websites::base
    $vhost = "releases.${::domain}"
    $vhostdir = "${websites::base::webdatadir}/${vhost}"
    $git_location = "git://git.${::domain}/web/releases"

    apache::vhost::base { $vhost:
        location => $vhostdir,
        options  => [ 'FollowSymLinks' ],
    }

    apache::vhost::base { "ssl_${vhost}":
        vhost    => $vhost,
        use_ssl  => true,
        location => $vhostdir,
        options  => [ 'FollowSymLinks' ],
    }

    git::snapshot { $vhostdir:
        source => $git_location,
    }
}
