class websites::nav {
    include websites::base
    $vhost = "nav.${::domain}"
    $vhostdir = "${websites::base::webdatadir}/${vhost}"
    $git_location = "git://git.${::domain}/web/nav"

    apache::vhost::base { $vhost:
        location => $vhostdir,
    }

    apache::vhost::base { "ssl_${vhost}":
        vhost    => $vhost,
        use_ssl  => true,
        location => $vhostdir,
    }

    git::snapshot { $vhostdir:
        source => $git_location,
    }

    file { "${vhostdir}/var/tmp/cache":
        ensure  => directory,
        mode    => '0660',
        group   => $apache::var::apache_group,
        require => Git::Snapshot[$vhostdir],
    }
}
