class shadow {
    include stdlib

    $login_defs = '/etc/login.defs'

    file { $login_defs:
        owner  => 'root',
        group  => 'shadow',
        mode   => '0640',
    }

    file_line { 'uid_max':
        path  => $login_defs,
       line  => 'UID_MAX                 2000',
       match => '^UID_MAX\s+',
    }

    file_line { 'gid_max':
        path  => $login_defs,
       line  => 'GID_MAX                 2000',
       match => '^GID_MAX\s+',
    }
}
