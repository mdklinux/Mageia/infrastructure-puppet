# for server where only admins can connect (allowed by default)
class access_classes::admin {
    class { 'pam::multiple_ldap_access':
        access_classes => []
    }
}


