class mga_buildsystem::config {
    class { 'buildsystem::var::signbot':
        keyid    => '80420F66',
        keyemail => "packages@${::domain}",
        keyname  => 'Mageia Packages',
    }

    class { 'buildsystem::var::groups':
        packagers            => 'mga-packagers',
        packagers_committers => 'mga-packagers-committers',
    }

    class { 'buildsystem::var::webstatus' :
        package_commit_url => "https://svnweb.${::domain}/packages?view=revision&revision=%d",
        theme_name         => 'mageia',
    }

    class { 'buildsystem::var::iurt':
        timeout_multiplier => $::architecture ? {
          /arm/ => 4,
          'aarch64' => 2,
          default => 1,
        }
    }

    class { 'buildsystem::var::scheduler' :
        admin_mail          => "sysadmin@group.${::domain}",
        build_nodes         => {
            'i586'   => [ 'ecosse0', 'rabbit0', 'ecosse1', 'rabbit1', 'rabbit2' ],
            'x86_64' => [ 'rabbit0', 'ecosse0', 'rabbit1', 'ecosse1', 'rabbit2' ],
            'armv7hl' => [ 'ociaa1-a', 'ociaa1-b'],
            'aarch64' => [ 'ociaa1-a', 'ociaa1-b'],
        },
        build_nodes_aliases => {
            'ecosse0' => "ecosse.${::domain}",
            'ecosse1' => "ecosse.${::domain}",
            'rabbit0' => "rabbit.${::domain}",
            'rabbit1' => "rabbit.${::domain}",
            'ec2aa1-a' => "ec2aa1.${::domain}",
            'ec2aa1-b' => "ec2aa1.${::domain}",
            'ec2aa2-a' => "ec2aa2.${::domain}",
            'ec2aa2-b' => "ec2aa2.${::domain}",
            'ec2aa3-a' => "ec2aa3.${::domain}",
            'ec2aa3-b' => "ec2aa3.${::domain}",
            'ec2x1-a' => "ec2x1.${::domain}",
            'ec2x1-b' => "ec2x1.${::domain}",
            'ec2x2-a' => "ec2x2.${::domain}",
            'ec2x2-b' => "ec2x2.${::domain}",
            'pktaa1-a' => "pktaa1.${::domain}",
            'pktaa1-b' => "pktaa1.${::domain}",
            'pktaa1-c' => "pktaa1.${::domain}",
            'pktaa1-d' => "pktaa1.${::domain}",
            'pktaa1-e' => "pktaa1.${::domain}",
            'pktaa1-f' => "pktaa1.${::domain}",
            'ociaa1-a' => "ociaa1.${::domain}",
            'ociaa1-b' => "ociaa1.${::domain}",
            'ociaa1-c' => "ociaa1.${::domain}",
            'ociaa2-a' => "ociaa2.${::domain}",
            'ociaa2-b' => "ociaa2.${::domain}",
            'ociaa2-c' => "ociaa2.${::domain}",
        },
        build_src_node      => 'duvel',
    }
    include buildsystem::var::repository
    class { 'buildsystem::var::binrepo':
        uploadmail_from => "root@${::domain}",
        uploadmail_to   => "packages-commits@ml.${::domain}",
    }

    $svn_hostname = "svn.${::domain}"
    $svn_root_packages = "svn://${svn_hostname}/svn/packages"
    $svn_root_packages_ssh = "svn+ssh://${svn_hostname}/svn/packages"
    class { 'buildsystem::var::mgarepo':
        submit_host           => "pkgsubmit.${::domain}",
        svn_hostname          => $svn_hostname,
        svn_root_packages     => $svn_root_packages,
        svn_root_packages_ssh => $svn_root_packages_ssh,
        oldurl                => "${svn_root_packages_ssh}/misc",
        conf                  => {
            'global' => {
                'ldap-server'       => "ldap.${::domain}",
                'ldap-base'         => "ou=People,${::dc_suffix}",
                'ldap-filterformat' => '(&(objectClass=inetOrgPerson)(uid=$username))',
                'ldap-resultformat' => '$cn <$mail>',
            }
        }
    }

    include stdlib

    $std_arch = ['x86_64', 'i586']
    $arm32_arch = ['armv5tl', 'armv7hl']
    $std_repos = {
        'release' => {
            'media_type'  => [ 'release' ],
            'requires'    => [],
            'order'       => 0,
        },
        'updates' => {
            'media_type'  => [ 'updates' ],
            'updates_for' => 'release',
            'requires'    => [ 'release' ],
            'order'       => 1,
        },
        'updates_testing' => {
            'media_type' => [ 'testing' ],
            'noauto'     => '1',
            'requires'   => [ 'updates' ],
            'order'      => 2,
        },
        'backports' => {
            'media_type' => [ 'backports' ],
            'noauto'     => '1',
            'requires'   => [ 'updates' ],
            'order'      => 3,
        },
        'backports_testing' => {
            'media_type' => [ 'testing' ],
            'noauto'     => '1',
            'requires'   => [ 'backports' ],
            'order'      => 4,
        },
    }
    $std_medias = {
        'core'    => {
            'repos'      => $std_repos,
            'media_type' => [ 'official', 'free' ],
            'order'      => 0,
        },
        'nonfree' => {
            'repos'      => $std_repos,
            'media_type' => [ 'official' ],
            'noauto'     => '1',
            'requires'   => [ 'core' ],
            'order'      => 1,
        },
        'tainted' => {
            'repos'      => $std_repos,
            'media_type' => [ 'official' ],
            'noauto'     => '1',
            'requires'   => [ 'core' ],
            'order'      => 2,
        },
    }
    $std_base_media = [ 'core/release', 'core/updates' ]
    $infra_medias = {
        'infra' => {
            'repos' => {
                'updates' => {
                    'media_type' => [ 'updates' ],
                    'requires'   => [ 'release' ],
                    'order'      => 0,
                },
            },
            'media_type' => [ 'infra' ],
            'requires'   => [ 'core' ],
            'order'      => 0,
        },
    }
    $std_macros = {
        'distsuffix'   => '.mga',
        'distribution' => 'Mageia',
        'vendor'       => 'Mageia.Org',
        '_real_vendor' => 'mageia',
    }
    $repo_allow_from_ips = [
        $::nodes_ipaddr[duvel][ipv6],
        $::nodes_ipaddr[duvel][ipv4],
        $::nodes_ipaddr[ecosse][ipv6],
        $::nodes_ipaddr[ecosse][ipv4],
        $::nodes_ipaddr[fiona][ipv6],
        $::nodes_ipaddr[fiona][ipv4],
        '10.42.0',
        $::nodes_ipaddr[rabbit][ipv4],
        $::nodes_ipaddr[rabbit][ipv6],
        $::nodes_ipaddr[sucuk][ipv4],
        $::nodes_ipaddr[sucuk][ipv6],
        '85.134.55.73',
        $::nodes_ipaddr[neru][ipv4],
        $::nodes_ipaddr[neru][ipv6],
        '2001:bc8:4400:2700::2729',
        '147.75.83.250',
	'2604:1380:2000:f100::1',
	'2a05:d014:e9:2c00::/56',
	'147.75.69.244/30',
	'2604:1380:1001:4900::/127',
        # Will be new neru
        '51.15.220.93',
        '2001:bc8:628:1f00::1',
        # Oracle cloud VMs
        '2603:c026:c101:f00::/64',
    ]
    $repo_allow_from_domains = [
        ".${::domain}",
    ]

    # the list of checks, actions, posts for cauldron in youri-upload
    $cauldron_youri_upload_targets = {
        'checks'  => [
            'version',
            'tag',
            'acl',
            'rpmlint',
            'recency',
        ],
        'actions' => [
            'markrelease',
            'sign',
            'install',
            'link',
            'unpack_release_notes',
            'unpack_gfxboot_theme',
            'unpack_meta_task',
            'unpack_installer_images',
            'unpack_installer_images_nonfree',
            'unpack_installer_stage2',
            'unpack_installer_advertising',
            'unpack_installer_rescue',
            'unpack_syslinux',
            'unpack_pci_usb_ids',
            'archive',
            'mail',
            'maintdb',
            'rebuild',
        ],
        'posts'   => [
            'genhdlist2_zstd',
            'createrepo_cauldron',
            'appstream_cauldron',
            'clean_rpmsrate',
            'mirror',
        ],
    }

    # TODO: mga >= 6 should use std config + createrepo, not a different one
    $mga6_youri_upload_targets = {
        'checks'  => [
            'version',
            'tag',
            'acl',
            'rpmlint',
            'recency',
        ],
        'actions' => [
            'sign',
            'install',
            'link',
            'archive',
            'mail',
        ],
        'posts'   => [
            'genhdlist2',
            'createrepo_mga6',
            'appstream_mga6',
            'mirror',
        ],
    }

    $mga7_youri_upload_targets = {
        'checks'  => [
            'version',
            'tag',
            'acl',
            'rpmlint',
            'recency',
        ],
        'actions' => [
            'sign',
            'install',
            'link',
            'archive',
            'mail',
        ],
        'posts'   => [
            'genhdlist2',
            'createrepo_mga7',
            'appstream_mga7',
            'mirror',
        ],
    }

    $mga8_youri_upload_targets = {
        'checks'  => [
            'version',
            'tag',
            'acl',
            'rpmlint',
            'recency',
        ],
        'actions' => [
            'sign',
            'install',
            'link',
            'archive',
            'mail',
        ],
        'posts'   => [
            'genhdlist2',
            'createrepo_mga8',
            'appstream_mga8',
            'mirror',
        ],
    }

    $mga9_youri_upload_targets = {
        'checks'  => [
            'version',
            'tag',
            'acl',
            'rpmlint',
            'recency',
        ],
        'actions' => [
            'sign',
            'install',
            'link',
            'archive',
            'mail',
        ],
        'posts'   => [
            'genhdlist2',
            'createrepo_mga9',
            'appstream_mga9',
            'mirror',
        ],
    }

    # the list of checks, actions, posts for infra distros in youri-upload
    $infra_youri_upload_targets = {
        'checks'  => [
            'version',
            'tag',
            'acl',
            'rpmlint',
            'recency',
        ],
        'actions' => [
            'sign',
            'install',
            'link',
            'archive',
        ],
        'posts'   => [
            'genhdlist2',
        ],
    }

    # the list of checks, actions, posts for cauldron in youri-todo
    $cauldron_youri_todo_targets = {
        'checks'  => [
            'source',
            'deps',
            'version',
            'tag',
            'acl',
            'host',
            'rpmlint',
            'recency',
            'queue_recency',
        ],
        'actions' => [
            'send',
            'dependencies',
            'rpminfo',
            'ulri',
        ],
    }

    # the list of checks, actions, posts for stable and infra distros in youri-todo
    $std_youri_todo_targets = {
        'checks'  => [
            'source',
            'version',
            'tag',
            'acl',
            'host',
            'rpmlint',
            'recency',
            'queue_recency',
        ],
        'actions' => [
            'send',
            'dependencies',
            'rpminfo',
            'ulri',
        ],
    }

    # rpmlint check options for stable and cauldron
    $mga_rpmlint = {
        'config'  => '/usr/share/rpmlint/config',
        'path'    => '/usr/bin/rpmlint',
        'results' => [
            'buildprereq-use',
            'no-description-tag',
            'no-summary-tag',
            'non-standard-group',
            'non-xdg-migrated-menu',
            'percent-in-conflicts',
            'percent-in-dependency',
            'percent-in-obsoletes',
            'percent-in-provides',
            'summary-ended-with-dot',
            'unexpanded-macro',
            'unknown-lsb-keyword',
            'malformed-line-in-lsb-comment-block',
            'empty-%postun',
            'empty-%post',
            'invalid-desktopfile',
            'standard-dir-owned-by-package',
            'use-tmp-in-%postun',
            'bogus-variable-use-in-%posttrans',
            'dir-or-file-in-usr-local',
            'dir-or-file-in-tmp',
            'dir-or-file-in-mnt',
            'dir-or-file-in-opt',
            'dir-or-file-in-home',
            'dir-or-file-in-var-local',
            'tmpfiles-conf-in-etc',
            'non-ghost-in-run',
            'non-ghost-in-var-run',
            'non-ghost-in-var-lock',
            'systemd-unit-in-etc',
            'udev-rule-in-etc',
        ],
    }

    # list of users allowed to submit packages when cauldron is frozen
    $cauldron_authorized_users = str_join(group_members('mga-release_managers'), '|')
    $cauldron_version_check = {
        'authorized_sections'   => '^[a-z]+/updates_testing$',
        #'authorized_sections' => 'none_section_authorized',
        #'authorized_packages' => 'none_package_authorized',
        'authorized_packages'   => 'drak|^(urpmi|perl-(MDK-Common|Gtk3|Glib(-Object-Introspection)?|URPM)|mgaonline|net_monitor|perl_checker|mandi|indexhtml|ldetect(-lst)?|msec|manatools|rpm-(mageia-setup|helper)|(mga-|mageia).*|iurt)$',
        'authorized_arches'     => 'none',
        'authorized_users'      => "^${cauldron_authorized_users}\$",
        'mode'                  => 'normal',
        #'mode'                  => 'version_freeze',
        #'mode'                  => 'freeze',
    }

    # for EOL distributions
    $frozen_version_check = {
        'authorized_packages' => 'none_package_authorized',
        'authorized_sections' => 'none_section_authorized',
        'authorized_arches'   => 'none',
        'mode'                => 'freeze',
    }

    # for supported stable distributions
    $std_version_check = {
        'authorized_packages' => 'none_package_authorized',
        'authorized_sections' => '^(core|nonfree|tainted)/(updates_testing|backports_testing)$',
        'authorized_arches'   => 'none',
        'mode'                => 'freeze',
    }

    $infra_authorized_users = str_join(group_members('mga-sysadmin'), '|')
    $infra_version_check = {
        'authorized_users' => "^${infra_authorized_users}\$",
        'mode'             => 'freeze',
    }

    class { 'buildsystem::var::distros':
        default_distro => 'cauldron',
        repo_allow_from_ips => $repo_allow_from_ips,
        repo_allow_from_domains => $repo_allow_from_domains,
        distros        => {
            'cauldron' => {
                'arch'                => concat($std_arch, ['armv7hl', 'aarch64']),
                'mandatory_arch'      => concat($std_arch, ['aarch64']),
                'no_media_cfg_update' => true,
                'medias'              => $std_medias,
                'base_media'          => $std_base_media,
                'branch'              => 'Devel',
                'version'             => '10',
                'submit_allowed'      => "${svn_root_packages}/cauldron",
                'macros'              => $std_macros,
                'youri'               => {
                    'upload' => {
                        'targets' => $cauldron_youri_upload_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                        },
                    },
                    'todo'   => {
                        'targets' => $cauldron_youri_todo_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                            'version' => $cauldron_version_check,
                        },
                    },
                },
            },


            '8'        => {
                'arch'                => concat($std_arch, ['armv7hl', 'aarch64']),
                'mandatory_arch'      => concat($std_arch, ['aarch64']),
                'no_media_cfg_update' => true,
                'medias'              => $std_medias,
                'base_media'          => $std_base_media,
                'branch'              => 'Official',
                'version'             => '8',
                'submit_allowed'      => "${svn_root_packages}/updates/8",
                'backports_allowed'   => "${svn_root_packages}/backports/8",
                'macros'              => $std_macros,
                'youri'               => {
                    'upload' => {
                        'targets' => $mga8_youri_upload_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                        },
                    },
                    'todo'   => {
                        'targets' => $std_youri_todo_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                            'version' => $std_version_check,
                        },
                    },
                },
            },

            '9'        => {
                'arch'                => concat($std_arch, ['armv7hl', 'aarch64']),
                'mandatory_arch'      => concat($std_arch, ['aarch64']),
                'no_media_cfg_update' => true,
                'medias'              => $std_medias,
                'base_media'          => $std_base_media,
                'branch'              => 'Official',
                'version'             => '9',
                'submit_allowed'      => "${svn_root_packages}/updates/9",
                'backports_allowed'   => "${svn_root_packages}/backports/9",
                'macros'              => $std_macros,
                'youri'               => {
                    'upload' => {
                        'targets' => $mga9_youri_upload_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                        },
                    },
                    'todo'   => {
                        'targets' => $std_youri_todo_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                            'version' => $std_version_check,
                        },
                    },
                },
            },

            'infra_8'  => {
                'arch'           => concat($std_arch, ['armv7hl', 'aarch64']),
                'medias'         => $infra_medias,
                'base_media'     => [ '8/core/release', '8/core/updates', 'infra/updates' ],
                'branch'         => 'Official',
                'version'        => '8',
                'submit_allowed' => "${svn_root_packages}/updates/infra_8",
                'macros'         => $std_macros,
                'based_on'       => {
                    '8' => {
                        'core' => [ 'release', 'updates' ],
                    },
                },
                'youri'          => {
                    'upload' => {
                        'targets' => $infra_youri_upload_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                        },
                    },
                    'todo'   => {
                        'targets' => $std_youri_todo_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                            'version' => $infra_version_check,
                        },
                    },
                },
                'no_mirror'      => true,
            },

            'infra_9'  => {
                'arch'           => concat($std_arch, ['armv7hl', 'aarch64']),
                'medias'         => $infra_medias,
                'base_media'     => [ '9/core/release', '9/core/updates', 'infra/updates' ],
                'branch'         => 'Official',
                'version'        => '9',
                'submit_allowed' => "${svn_root_packages}/updates/infra_9",
                'macros'         => $std_macros,
                'based_on'       => {
                    '9' => {
                        'core' => [ 'release', 'updates' ],
                    },
                },
                'youri'          => {
                    'upload' => {
                        'targets' => $infra_youri_upload_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                        },
                    },
                    'todo'   => {
                        'targets' => $std_youri_todo_targets,
                        'checks'  => {
                            'rpmlint' => $mga_rpmlint,
                            'version' => $infra_version_check,
                        },
                    },
                },
                'no_mirror'      => true,
            },
        }
    }
    $checks_tag_options = {
        'tags' => {
            'release'      => inline_template("^[^~]*<%= std_macros['distsuffix'] %>\\d+"),
            'distribution' => inline_template("^<%= std_macros['distribution'] %>"),
            'vendor'       => inline_template("^<%= std_macros['vendor'] %>$"),
        },
    }
    class { 'buildsystem::var::youri':
        packages_archivedir => "${buildsystem::var::scheduler::homedir}/old",
        youri_conf          => {
            'upload' => {
                'checks'  => {
                    'tag'     => {
                        'options' => $checks_tag_options,
                    },
                    'rpmlint' => {
                        'options' => {
                            'results' => [
                                'buildprereq-use',
                                'no-description-tag',
                                'no-summary-tag',
                                'non-standard-group',
                                'non-xdg-migrated-menu',
                                'percent-in-conflicts',
                                'percent-in-dependency',
                                'percent-in-obsoletes',
                                'percent-in-provides',
                                'summary-ended-with-dot',
                                'unexpanded-macro',
                                'unknown-lsb-keyword',
                                'malformed-line-in-lsb-comment-block',
                                'empty-%postun',
                                'empty-%post',
                                'invalid-desktopfile',
                                'standard-dir-owned-by-package',
                                'use-tmp-in-%postun',
                                'bogus-variable-use-in-%posttrans',
                                'dir-or-file-in-usr-local',
                                'dir-or-file-in-tmp',
                                'dir-or-file-in-mnt',
                                'dir-or-file-in-opt',
                                'dir-or-file-in-home',
                                'dir-or-file-in-var-local',
                            ],
                        },
                    },
                },
                'actions' => {
                    'mail' => {
                        'options' => {
                            'to'       => "changelog@ml.${::domain}",
                            'reply_to' => "dev@ml.${::domain}",
                            'from'     => "buildsystem-daemon@${::domain}",
                            'prefix'   => 'RPM',
                        },
                    },
                },
            },
            'todo'   => {
                'checks' => {
                    'tag' => {
                        'options' => $checks_tag_options,
                    },
                },
            },
        }
    }
}
