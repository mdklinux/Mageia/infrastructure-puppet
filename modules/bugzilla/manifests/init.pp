class bugzilla {

    $bugzilla_location = '/usr/share/bugzilla'

    package {['graphviz',
              'perl-Template-GD', # needed for graphical_report support
              'perl-Test-Taint',
              'perl-JSON-RPC',
              'perl-JSON-XS',
              'perl-Email-MIME',
              'perl-Email-Sender',
              'perl-Math-Random-ISAAC',
              'perl-Chart',
              'perl-PatchReader',
              'perl-ldap',
              'perl-SOAP-Lite',
              'perl-XMLRPC-Lite',
              'perl-CGI',
              'perl-HTML-Scrubber',
              'perl-Encode-Detect',
              'perl-File-MimeInfo',
              'perl-Email-Reply',
              'perl-HTML-FormatText-WithLinks',
              'perl-Cache-Memcached',
              'perl-File-Copy-Recursive',
              'perl-Daemon-Generic']: }

    $pgsql_password = extlookup('bugzilla_pgsql','x')
    $ldap_password = extlookup('bugzilla_ldap','x')
    $bugzilla_secret_key = extlookup('bugzilla_secret_key','x')

    postgresql::remote_db_and_user { 'bugs':
        description => 'Bugzilla database',
        password    => $pgsql_password,
    }

    file { "$bugzilla_location/localconfig":
      group   => 'apache',
      mode    => '0640',
      content => template('bugzilla/localconfig')
    }


    file { "$bugzilla_location/data/params.json":
      group   => 'apache',
      mode    => '0640',
      content => template('bugzilla/params.json')
    }
    file { "$bugzilla_location/graphs":
      ensure => directory,
      owner => 'apache',
      group => 'apache',
      mode  => '0770'
    }

    apache::webapp_other { 'bugzilla':
        webapp_file => 'bugzilla/webapp_bugzilla.conf',
    }

    $bugs_vhost = "bugs.${::domain}"
    $vhost_root = $bugzilla_location

    apache::vhost::redirect_ssl { $bugs_vhost: }

    apache::vhost::base { $bugs_vhost:
        content  => template('bugzilla/vhost.conf'),
        aliases  => { '/bugzilla/' => $vhost_root },
        use_ssl  => true,
        location => $vhost_root,
        vhost    => $bugs_vhost,
        enable_location => false,
    }

    git::snapshot { $bugzilla_location:
      source  => "git://git.${::domain}/web/bugs"
    }

    file { 'Mageia':
      ensure  => directory,
      path    => $bugzilla_location,
      group   => 'apache',
      recurse => true,
      require => Git::Snapshot[$bugzilla_location],
    }

    file { "$bugzilla_location/data":
        ensure => directory,
        owner => 'apache',
        group => 'apache',
        mode  => '0770'
    }

    file { "$bugzilla_location/data/assets":
        ensure => directory,
        owner => 'apache',
        group => 'apache',
        mode  => '0770'
    }

    file { "$bugzilla_location/robots.txt":
      group   => 'apache',
      mode    => '0640',
      content => template('bugzilla/robots.txt')
    }

   file { "$bugzilla_location/data/bugzilla-update.xml":
      owner => 'apache',
      group => 'apache',
      mode  => '0640'
    }


    file { [
                "$bugzilla_location/admin.cgi",
                "$bugzilla_location/attachment.cgi",
                "$bugzilla_location/buglist.cgi",
                "$bugzilla_location/chart.cgi",
                "$bugzilla_location/colchange.cgi",
                "$bugzilla_location/config.cgi",
                "$bugzilla_location/createaccount.cgi",
                "$bugzilla_location/describecomponents.cgi",
                "$bugzilla_location/describekeywords.cgi",
                "$bugzilla_location/duplicates.cgi",
                "$bugzilla_location/editclassifications.cgi",
                "$bugzilla_location/editfields.cgi",
                "$bugzilla_location/editgroups.cgi",
                "$bugzilla_location/editmilestones.cgi",
                "$bugzilla_location/editproducts.cgi",
                "$bugzilla_location/editusers.cgi",
                "$bugzilla_location/editversions.cgi",
                "$bugzilla_location/editworkflow.cgi",
                "$bugzilla_location/editcomponents.cgi",
                "$bugzilla_location/editflagtypes.cgi",
                "$bugzilla_location/editkeywords.cgi",
                "$bugzilla_location/editparams.cgi",
                "$bugzilla_location/editsettings.cgi",
                "$bugzilla_location/editvalues.cgi",
                "$bugzilla_location/editwhines.cgi",
                "$bugzilla_location/enter_bug.cgi",
                "$bugzilla_location/index.cgi",
                "$bugzilla_location/jsonrpc.cgi",
                "$bugzilla_location/page.cgi",
                "$bugzilla_location/post_bug.cgi",
                "$bugzilla_location/process_bug.cgi",
                "$bugzilla_location/query.cgi",
                "$bugzilla_location/quips.cgi",
                "$bugzilla_location/relogin.cgi",
                "$bugzilla_location/reports.cgi",
                "$bugzilla_location/rest.cgi",
                "$bugzilla_location/search_plugin.cgi",
                "$bugzilla_location/show_bug.cgi",
                "$bugzilla_location/showdependencytree.cgi",
                "$bugzilla_location/testagent.cgi",
                "$bugzilla_location/userprefs.cgi",
                "$bugzilla_location/xmlrpc.cgi",
                "$bugzilla_location/report.cgi",
                "$bugzilla_location/request.cgi",
                "$bugzilla_location/sanitycheck.cgi",
                "$bugzilla_location/show_activity.cgi",
                "$bugzilla_location/showdependencygraph.cgi",
                "$bugzilla_location/summarize_time.cgi",
                "$bugzilla_location/token.cgi",
                "$bugzilla_location/votes.cgi",
                "$bugzilla_location/checksetup.pl",
                "$bugzilla_location/clean-bug-user-last-visit.pl",
                "$bugzilla_location/collectstats.pl",
                "$bugzilla_location/email_in.pl",
                "$bugzilla_location/importxml.pl",
                "$bugzilla_location/install-module.pl",
                "$bugzilla_location/jobqueue.pl",
                "$bugzilla_location/migrate.pl",
                "$bugzilla_location/runtests.pl",
                "$bugzilla_location/sanitycheck.pl",
                "$bugzilla_location/testserver.pl",
                "$bugzilla_location/whineatnews.pl",
                "$bugzilla_location/whine.pl",
	   ]:
      group   => 'apache',
      mode    => '0750',
    }

    cron { 'collectstats':
        command => "cd $bugzilla_location && ./collectstats.pl",
        user    => 'apache',
        hour    => 2,
        minute  => 30,
    }

    cron { 'clean-bug-user-last-visit':
        command => "cd $bugzilla_location && ./clean-bug-user-last-visit.pl",
        user    => 'apache',
        hour    => 3,
        minute  => 0,
    }
    cron { 'sanitycheck':
        command => "cd $bugzilla_location && $bugzilla_location/sanitycheck.pl --login LpSolit@gmail.com",
        user    => 'apache',
        hour    => 21,
        minute  => 0,
    }

}
