class mga-mirrors {

    $vhost = "mirrors.${::domain}"
    $mirrors_dir = '/var/www/mirrors'

    package { 'mga-mirrors': }

    apache::vhost::catalyst_app { $vhost:
        script  => '/usr/bin/mga_mirrors_fastcgi.pl',
        require => Package['mga-mirrors'],
        aliases => {
            '/status' => '/var/www/mirrors/status.html',
        }
    }

    apache::vhost::catalyst_app { "ssl_${vhost}":
        script  => '/usr/bin/mga_mirrors_fastcgi.pl',
        require => Package['mga-mirrors'],
        vhost   => $vhost,
        use_ssl => true,
        aliases => {
            '/status' => '/var/www/mirrors/status.html',
        },
    }

    $pgsql_password = extlookup('mga_mirror_pgsql','x')

    postgresql::remote_db_and_user { 'mirrors':
        password    => $pgsql_password,
        description => 'Mirrors database',
    }

    file { '/etc/mga-mirrors.ini':
        group   => 'apache',
        mode    => '0640',
        content => template('mga-mirrors/mga-mirrors.ini'),
        require => Package['mga-mirrors']
    }

    file { '/etc/cron.d/check_mga_mirrors':
        content => template('mga-mirrors/cron-mga_mirrors'),
        require => Package['mga-mirrors']
    }

    file { $mirrors_dir:
        ensure => directory,
	owner  => 'nobody',
    }

    file { '/usr/local/bin/check_mirrors_status':
        mode   => '0755',
        source => 'puppet:///modules/mga-mirrors/check_mirrors_status',
    }
}
