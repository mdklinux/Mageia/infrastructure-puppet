class postfix::simple_relay inherits postfix {
    File['/etc/postfix/main.cf'] {
        content => template('postfix/simple_relay_main.cf'),
    }
    file {
        '/etc/postfix/sympa_aliases':
            content => template('postfix/sympa_aliases');
    }
}
