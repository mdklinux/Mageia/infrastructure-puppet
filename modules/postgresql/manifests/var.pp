class postgresql::var {

  $pgsql_data = '/var/lib/pgsql/data/'

  if versioncmp($::lsbdistrelease, '5') < 0 {
    $pg_version = '9.0'
  } else {
    $pg_version = '9.6'
  }

  $hba_file = "${pgsql_data}/pg_hba.conf"
}
# vim: sw=2
