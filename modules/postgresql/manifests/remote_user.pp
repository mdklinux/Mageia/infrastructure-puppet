# FIXME: In puppet >3.0 word 'tag' is reserved, so it have to be renamed
define postgresql::remote_user( $password,
                                $tag = 'default') {
    @@postgresql::user { $name:
        tag      => $tag,
        password => $password,
    }
}


