# FIXME: In puppet >3.0 word 'tag' is reserved, so it have to be renamed
define postgresql::tagged() {
    # TODO add a system of tag so we can declare database on more than one
    # server
    Postgresql::User <<| tag == $name |>>
    Postgresql::Database <<| tag == $name |>>
    Postgresql::Db_and_user <<| tag == $name |>>
}
