class gitmirror {

    $git_dir     = '/git'
    $git_login   = 'git'
    $git_homedir = "/var/lib/${git_login}"
    $git_rundir  = '/var/run/on-the-pull'

    group { $git_login:
      ensure => present,
    }

    user { $git_login:
      ensure      => present,
      home        => $git_homedir,
    }

    file { $git_dir:
      ensure => directory,
      owner  => $git_login,
      group  => $git_login,
      mode   => '0755',
    }

    file { $git_rundir:
      ensure => directory,
      mode   => '0755',
    }

    mga_common::local_script { 'on-the-pull':
        content => template('gitmirror/on-the-pull'),
    }

    file { '/etc/init.d/on-the-pull':
        content => template('gitmirror/on-the-pull.init'),
        mode    => '0755',
    }

    service { 'on-the-pull':
        require  => [
            Mga_common::Local_script["on-the-pull"],
            File['/etc/init.d/on-the-pull'],
        ],
    }

    mga_common::local_script { 'gitmirror-sync-metadata':
        content => template('gitmirror/rsync-metadata.sh'),
    }
}
