#!/bin/bash

REPO="$1"
GITROOT="/git"
RSYNCROOT="rsync://duvel.mageia.org/git"

if [ ! -d "$GITROOT/$REPO" ]; then
	echo "No repository found $REPO" >&2
	exit 1
fi

/usr/bin/rsync -a --include="description" --include="info" --include="info/web" --include="info/web/last-modified" --exclude="*" "$RSYNCROOT/$REPO/" "$GITROOT/$REPO/"
/usr/bin/rsync -a "$RSYNCROOT/$REPO/config" "$GITROOT/$REPO/config.upstream"

OWNER=$(git config --file "$GITROOT/$REPO/config.upstream" gitweb.owner)
DESC=$(git config --file "$GITROOT/$REPO/config.upstream" gitweb.description)
rm -f "$GITROOT/$REPO/config.upstream"

CUROWNER=$(git config --file "$GITROOT/$REPO/config" gitweb.owner)
if [ "$CUROWNER" != "$OWNER" ]; then
	git config --file "$GITROOT/$REPO/config" gitweb.owner "$OWNER"
fi

CURDESC=$(git config --file "$GITROOT/$REPO/config" gitweb.description)
if [ "$CURDESC" != "$DESC" ]; then
	git config --file "$GITROOT/$REPO/config" gitweb.description "$DESC"
fi
