# vim:ft=yaml:et:sw=4

# helper variables
mirror: http://repository.mageia.org/distrib/8
mirror_i586: ${mirror}/i586/media
mirror_x86_64: ${mirror}/x86_64/media

# resultset definition
resultset:
    class: Youri::Check::Resultset::DBI
    options:
        driver: Pg
        host: <%= pgsql_server %>;sslmode=require
        base: <%= pgsql_db %>
        user: <%= pgsql_user %>
        pass: <%= pgsql_password %>

resolver:
    class: Youri::Check::Maintainer::Resolver::CGI
    options:
        url: http://pkgsubmit.<%= domain %>/data/maintdb.txt
        exceptions:
            - nobody


# checks definitions
tests:
    dependencies:
        class: Youri::Check::Test::Dependencies

    missing:
        class: Youri::Check::Test::Missing

# reports definitions
reports:
    file:
        class: Youri::Check::Report::File
        options:
            to: <%= outdir %>
            global: 1
            individual: 1
            formats:
                html:
                    class: Youri::Check::Report::Format::HTML
                text:
                    class: Youri::Check::Report::Format::Text
                rss:
                    class: Youri::Check::Report::Format::RSS

# media definitions
medias:
    core.i586:
        class: Youri::Media::URPM
        options:
            name: core
            type: binary
            hdlist: ${mirror_i586}/media_info/hdlist_core.cz
            options:
                dependencies:
                    allowed:
                        - core.i586
                missing:
                    allowed:
                        - core.sources

    core_updates.i586:
        class: Youri::Media::URPM
        options:
            name: core_updates
            type: binary
            hdlist: ${mirror_i586}/media_info/hdlist_core_updates.cz
            options:
                dependencies:
                    allowed:
                        - core.i586
                        - core_updates.i586
                missing:
                    allowed:
                        - core.sources
                        - core_updates.sources

    core_updates_testing.i586:
        class: Youri::Media::URPM
        options:
            name: core_updates_testing
            type: binary
            hdlist: ${mirror_i586}/media_info/hdlist_core_updates_testing.cz
            options:
                dependencies:
                    allowed:
                        - core.i586
                        - core_updates.i586
                        - core_updates_testing.i586
                missing:
                    allowed:
                        - core.sources
                        - core_updates.sources
                        - core_updates_testing.sources

    core.x86_64:
        class: Youri::Media::URPM
        options:
            name: core
            type: binary
            hdlist: ${mirror_x86_64}/media_info/hdlist_core.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - core.i586
                missing:
                    allowed:
                        - core.sources

    core_updates.x86_64:
        class: Youri::Media::URPM
        options:
            name: core_updates
            type: binary
            hdlist: ${mirror_x86_64}/media_info/hdlist_core_updates.cz
            options:
                dependencies:
                    allowed:
                        - core.i586
                        - core_updates.i586
                        - core.x86_64
                        - core_updates.x86_64
                missing:
                    allowed:
                        - core.sources
                        - core_updates.sources

    core_updates_testing.x86_64:
        class: Youri::Media::URPM
        options:
            name: core_updates_testing
            type: binary
            hdlist: ${mirror_x86_64}/media_info/hdlist_core_updates_testing.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - core_updates.x86_64
                        - core_updates_testing.x86_64
                        - core.i586
                        - core_updates.i586
                        - core_updates_testing.i586
                missing:
                    allowed:
                        - core.sources
                        - core_updates.sources
                        - core_updates_testing.sources

    core.sources:
        class: Youri::Media::URPM
        options:
            name: core
            type: source
            hdlist: ${mirror_i586}/media_info/hdlist_core.src.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - core.i586

    core_updates.sources:
        class: Youri::Media::URPM
        options:
            name: core_updates
            type: source
            hdlist: ${mirror_i586}/media_info/hdlist_core_updates.src.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - core_updates.x86_64
                        - core.i586
                        - core_updates.i586

    core_updates_testing.sources:
        class: Youri::Media::URPM
        options:
            name: core_updates_testing
            type: source
            hdlist: ${mirror_i586}/media_info/hdlist_core_updates_testing.src.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - core_updates.x86_64
                        - core_updates_testing.x86_64
                        - core.i586
                        - core_updates.i586
                        - core_updates_testing.i586

    nonfree.i586:
        class: Youri::Media::URPM
        options:
            name: nonfree
            type: binary
            hdlist: ${mirror_i586}/media_info/hdlist_nonfree_release.cz
            options:
                dependencies:
                    allowed:
                        - core.i586
                        - nonfree.i586
                missing:
                    allowed:
                        - nonfree.sources

    nonfree.x86_64:
        class: Youri::Media::URPM
        options:
            name: nonfree
            type: binary
            hdlist: ${mirror_x86_64}/media_info/hdlist_nonfree_release.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - core.i586
                        - nonfree.x86_64
                        - nonfree.i586
                missing:
                    allowed:
                        - nonfree.sources


    nonfree.sources:
        class: Youri::Media::URPM
        options:
            name: nonfree
            type: source
            hdlist: ${mirror_i586}/media_info/hdlist_nonfree_release.src.cz
            options:
                dependencies:
                    allowed:
                        - core.x86_64
                        - nonfree.x86_64
                        - core.i586
                        - nonfree.i586
