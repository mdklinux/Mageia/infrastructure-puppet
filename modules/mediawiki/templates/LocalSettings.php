<?php

# This file was created by puppet, so any change will be overwritten

# See includes/DefaultSettings.php for all configurable settings
# and their default values, but don't forget to make changes in _this_
# file, not there.
#
# Further documentation for configuration settings may be found at:
# https://www.mediawiki.org/wiki/Manual:Configuration_settings

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
        exit;
}

## Installation path (should default to this value, but define for clarity)
$IP = '/usr/share/mediawiki';

## Include path necessary to load LDAP module
$path = array( $IP, "$IP/includes", "$IP/languages" );
set_include_path( implode( PATH_SEPARATOR, $path ) . PATH_SEPARATOR . get_include_path() );

## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;

$wgSitename = "<%= title %>";
# $wgMetaNamespace = ""; # Defaults to $wgSitename

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "/<%= path %>";

## The protocol and server name to use in fully-qualified URLs
$wgServer = "https://wiki.mageia.org";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## The relative URL path to the skins directory
$wgStylePath = "$wgScriptPath/skins";

## The relative URL path to the logo.  Make sure you change this from the default,
## or else you'll overwrite your logo when you upgrade!
$wgLogo = "$wgStylePath/common/images/wiki_mga.png";

## UPO means: this is also a user preference option

$wgEnableEmail = true;
$wgEnableUserEmail = true; # UPO

$wgEmergencyContact = "root@<%= domain %>";
$wgPasswordSender = "root@<%= domain %>";

$wgEnotifUserTalk = true; # UPO
$wgEnotifWatchlist = true; # UPO
$wgEmailAuthentication = true;

## Database settings
$wgDBtype = "postgres";
$wgDBserver = "pg.<%= domain %>";
$wgDBname = "<%= db_name %>";
$wgDBuser = "<%= db_user %>";
$wgDBpassword = "<%= db_password %>";

# Postgres specific settings
$wgDBport = "5432";
$wgDBmwschema = "mediawiki";
$wgDBts2schema = "public";

## Shared memory settings
$wgMainCacheType = CACHE_NONE;
$wgMemCachedServers = [];

## To enable image uploads, make sure the 'images' directory
## is writable, then set this to true:
$wgEnableUploads = true;
# use gd, as convert do not work for big image
# see https://bugs.mageia.org/show_bug.cgi?id=3202
$wgUseImageMagick = true;
#$wgImageMagickConvertCommand = "/usr/bin/convert";

# InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = false;

## If you use ImageMagick (or any other shell command) on a
## Linux server, this will need to be set to the name of an
## available UTF-8 locale
$wgShellLocale = "en_US.UTF-8";

## Set $wgCacheDirectory to a writable directory on the web server
## to make your wiki go slightly faster. The directory should not
## be publically accessible from the web.
# This seems actually mandatory to get the Vector skin to work properly
# https://serverfault.com/a/744059
# FIXME: Dehardcode that path (maybe via ${wiki_root} if exposed?)
$wgCacheDirectory = "/srv/wiki/<%= path %>/cache";

$wgUploadDirectory = "/srv/wiki/<%= path %>/images";

# This seems mandatory to get the Vector skin to work properly
# https://phabricator.wikimedia.org/T119934
# FIXME: Dehardcode that path (maybe via ${wiki_root} if exposed?)
$wgTmpDirectory = "/srv/wiki/<%= path %>/tmp";

# Array of interwiki prefixes for current wiki.
$wgLocalInterwikis = array( strtolower( $wgSitename ) );

# Site language code, should be one of the list in ./languages/data/Names.php
$wgLanguageCode = "<%= lang %>";

$wgSecretKey = "<%= secret_key %>";

# Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";

# Site upgrade key. Must be set to a string (default provided) to turn on the
# web installer while LocalSettings.php is in place
# FIXME: This should be set to a secure value:
# https://www.mediawiki.org/wiki/Manual:$wgUpgradeKey
# $wgUpgradeKey = "";

## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgEnableCreativeCommonsRdf = true;
# TODO add a proper page
$wgRightsPage = ""; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "https://creativecommons.org/licenses/by-sa/3.0/";
$wgRightsText = "Creative Commons - Attribution-ShareAlike 3.0 Unported";
# TODO get the icon to host it on our server
$wgRightsIcon = "https://licensebuttons.net/l/by-sa/3.0/88x31.png";

# Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";

## Default skin: you can change the default skin. Use the internal symbolic
## names, ie 'vector', 'monobook':
$wgDefaultSkin = 'vector';

# Enabled skins.
# The following skins were automatically enabled:
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Vector' );


# End of automatically generated settings.
# Add more configuration options below.


# Setting this to true will invalidate all cached pages whenever
# LocalSettings.php is changed.
$wgInvalidateCacheOnLocalSettingsChange = true;

# FIXME: Obsoleted, to be replaced by $wgPasswordPolicy
# https://www.mediawiki.org/wiki/Manual:$wgPasswordPolicy
$wgMinimalPasswordLength = 1;

# Give more details on errors
$wgShowExceptionDetails = true;


## LDAP setup

require_once 'extensions/LdapAuthentication/LdapAuthentication.php';
$wgAuth = new LdapAuthenticationPlugin();

## uncomment to debug
# $wgLDAPDebug = 10;
# $wgDebugLogGroups["ldap"] = "/tmp/wiki_ldap.log";
#
$wgDebugLogFile = "/tmp/wiki.log";
#

$wgLDAPUseLocal = false;

$wgLDAPDomainNames = array( 'ldap' );

# TODO make it workable with more than one server
$wgLDAPServerNames = array( 'ldap' => 'ldap.<%= domain %>' );

$wgLDAPSearchStrings = array( 'ldap' => 'uid=USER-NAME,ou=People,<%= dc_suffix %>' );

$wgLDAPEncryptionType = array( 'ldap' => 'tls' );

$wgLDAPBaseDNs = array( 'ldap' => '<%= dc_suffix %>' );
$wgLDAPUserBaseDNs = array( 'ldap' => 'ou=People,<%= dc_suffix %>' );
$wgLDAPGroupBaseDNs = array ( 'ldap' => 'ou=Group,<%= dc_suffix %>' );

$wgLDAPProxyAgent = array( 'ldap' => 'cn=mediawiki-alamut,ou=System Accounts,<%= dc_suffix %>' );

$wgLDAPProxyAgentPassword = array( 'ldap' => '<%= ldap_password %>' );

$wgLDAPUseLDAPGroups = array( 'ldap' => true );
$wgLDAPGroupNameAttribute = array( 'ldap' => 'cn' );
$wgLDAPGroupUseFullDN = array( 'ldap' => true );
$wgLDAPLowerCaseUsername = array( 'ldap' => true );
$wgLDAPGroupObjectclass = array( 'ldap' => 'posixGroup' );
$wgLDAPGroupAttribute = array( 'ldap' => 'member' );

$wgLDAPLowerCaseUsername = array( 'ldap' => true );

$wgLDAPPreferences = array( 'ldap' => array( 'email'=>'mail','realname'=>'cn','nickname'=>'uid','language'=>'preferredlanguage') );

<%= wiki_settings %>
