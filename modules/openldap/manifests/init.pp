class openldap {
    include openldap::var

    package { 'openldap-servers': }

    service { $openldap::var::service:
        subscribe => Package['openldap-servers'],
        require   => Openssl::Self_signed_cert["ldap.${::domain}"],
    }

    exec { "/etc/init.d/${openldap::var::service} check":
        refreshonly => true,
        notify      => Service[$openldap::var::service],
    }

    file { '/etc/ssl/openldap/':
        ensure => directory,
    }

    openssl::self_signed_cert{ "ldap.${::domain}":
        directory => '/etc/ssl/openldap/',
    }

    openldap::config {
        '/etc/openldap/slapd.conf':
            content => '';
        '/etc/openldap/mandriva-dit-access.conf':
            content => '';
        '/etc/sysconfig/ldap':
            content => '';
    }
}
