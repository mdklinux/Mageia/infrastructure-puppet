class openldap::var {
    if versioncmp($::lsbdistrelease, '4') < 0 {
        $service = 'ldap'
    } else {
        $service = 'slapd'
    }
}
