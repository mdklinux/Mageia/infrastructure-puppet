class pam::multiple_ldap_access($access_classes, $restricted_shell = false) {
    include stdlib

    $default_access_classes = [ 'mga-sysadmin', 'mga-unrestricted_shell_access' ]
    if empty($access_classes) {
        $allowed_access_classes = $default_access_classes
    } else {
        $allowed_access_classes = concat($default_access_classes, $access_classes)
    }

    if $restricted_shell {
        include restrictshell
    }
    include pam::base
}
