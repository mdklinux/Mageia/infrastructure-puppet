define apache::config($content) {
    file { $name:
        content => $content,
        notify  => Exec['apachectl configtest'],
    }
}
