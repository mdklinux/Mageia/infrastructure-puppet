class apache::mod::php {
    include apache::base
    $php_date_timezone = 'UTC'

    package { 'apache-mod_php': }

    apache::config { "${apache::base::conf_d}/mod_php.conf":
        content => template('apache/mod/php.conf'),
    }
}
