class apache::base {
    include apache::var

    # apache-mpm-prefork is merged from mga3 up
    $apache_server = $lsbdistrelease ? {
        /1|2/   => 'apache-mpm-prefork',
        default => 'apache',
    }

    package { $apache_server:
        alias => 'apache-server',
    }

    if versioncmp($::lsbdistrelease, '2') <= 0 {
        $conf_d = '/etc/httpd/conf.d'

        # only needed on mga1 and mga2
        package { $apache::var::pkg_conf: }
    } else {
        $conf_d = '/etc/httpd/conf/conf.d'
    }

    service { 'httpd':
        alias     => 'apache',
        subscribe => [ Package['apache-server'] ],
    }

    exec { 'apachectl configtest':
        refreshonly => true,
        notify      => Service['apache'],
    }

    apache::config {
        "${conf_d}/no_hidden_file_dir.conf":
            content => template('apache/no_hidden_file_dir.conf'),
            require => Package[$apache::var::pkg_conf];
        "${conf_d}/customization.conf":
            content => template('apache/customization.conf'),
            require => Package[$apache::var::pkg_conf];
        '/etc/httpd/conf/vhosts.d/00_default_vhosts.conf':
            content => template('apache/00_default_vhosts.conf'),
            require => Package[$apache::var::pkg_conf];
    }

    file { '/etc/logrotate.d/httpd':
        content => template('apache/logrotate')
    }
}
