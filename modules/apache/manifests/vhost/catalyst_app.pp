define apache::vhost::catalyst_app( $script,
                                    $location = '',
                                    $process = 4,
                                    $use_ssl = false,
                                    $aliases = {},
                                    $vhost = false) {
    include apache::mod::fcgid
    if ($location) {
      $aliases['/static'] = "${location}/root/static"
    }

    $script_aliases = {
        '/' => "$script/",
    }

    apache::vhost::base { $name:
        vhost   => $vhost,
        use_ssl => $use_ssl,
        content => template('apache/vhost_fcgid.conf'),
        aliases => $aliases,
    }
}


