define catdap::snapshot($location, $git_location, $git_branch = 'master') {
    file { "${location}/catdap_local.yml":
        group   => apache,
        mode    => '0640',
        content => template('catdap/catdap_local.yml'),
        require => Git::Snapshot[$location],
    }

    git::snapshot { $location:
        source => $git_location,
        branch => $git_branch,
    }

    apache::vhost::catalyst_app { $name:
        script   => "${location}/script/catdap_fastcgi.pl",
        location => $location,
        use_ssl  => true,
    }

    apache::vhost::redirect_ssl { $name: }
}
