# name: ttyS0
define serial_console::serial_console() {
    service { "serial-getty@${name}":
        provider => systemd,
        ensure => running,
        enable => true,
    }
}
