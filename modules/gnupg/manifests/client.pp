class gnupg::client {
if versioncmp($::lsbdistrelease, '7') < 0 {
    package {['gnupg',
              'rng-utils']:
    }
} else {
    package {['gnupg2',
              'rng-utils']:
    }
}

    mga_common::local_script { 'create_gnupg_keys.sh':
        content => template('gnupg/create_gnupg_keys.sh')
    }
}


