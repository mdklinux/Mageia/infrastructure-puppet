class mirror::base {
    $locksdir = '/home/mirror/locks'

    file { $locksdir:
        ensure => directory,
        owner  => 'mirror',
        group  => 'mirror',
    }

    group { 'mirror': }

    user { 'mirror':
        gid     => 'mirror',
    }
}
