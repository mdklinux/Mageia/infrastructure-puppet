class mirrorbrain {

    $mb_user  = 'mirrorbrain'
    $mb_home  = "/var/lib/${mb_user}"
    $mb_repo  = "${mb_home}/mirror"
    $mb_vhost = "dl.${::domain}"

    $mb_pgsql_pw = extlookup('mirrorbrain_pgsql','x')

    group { $mb_user:
      ensure => present
    }

    user { $mb_user:
      ensure => present,
      home   => $mb_home
    }

    file { $mb_home:
      ensure => directory,
      owner  => $mb_user,
      group  => $mb_user,
      mode   => '0751'
    }

    file { $mb_repo:
      ensure => directory,
      owner  => $mb_user,
      group  => $mb_user,
      mode   => '0755'
    }

    package {['mirrorbrain',
              'mirrorbrain-scanner',
              'mirrorbrain-tools',
              'apache-mod_mirrorbrain',
              'apache-mod_dbd']: }


    postgresql::remote_db_and_user { 'mirrorbrain':
        description => 'Mirrorbrain database',
        password    => $mb_pgsql_pw,
    }

    file { '/etc/httpd/conf/geoip.conf':
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('mirrorbrain/geoip.conf')
    }

    file { '/etc/httpd/conf/modules.d/11-mirrorbrain.conf':
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('mirrorbrain/mod_mirrorbrain.conf')
    }

    file { '/etc/mirrorbrain.conf':
      owner   => 'root',
      group   => "$mb_user",
      mode    => '0640',
      content => template('mirrorbrain/mirrorbrain.conf')
    }

    apache::vhost::base { "${mb_vhost}":
        vhost    => "${mb_vhost}",
        location => "${mb_repo}"
    }

    apache::vhost::base { "ssl_${mb_vhost}":
        vhost    => "${mb_vhost}",
        use_ssl  => true,
        location => "${mb_repo}"
    }

    apache::webapp_other { 'mirrorbrain':
        webapp_file => 'mirrorbrain/webapp.conf',
    }

    # Update GeoIP db
    cron { 'MirrorBrain: weekly GeoIP update':
        command => 'sleep $(($RANDOM/1024)); /usr/bin/geoip-lite-update',
        user    => 'root',
        minute  => 30,
        hour    => 3,
        weekday => 0
    }

    # distrib tree
    # mga 1-4 are frozen, so only one manual run has been done
    # distrib/5 still active
    cron { 'MirrorBrain: Sync Mga 5 every 4 hours ':
        command => "/usr/bin/null-rsync rsync.mageia.org::mageia/distrib/5 ${mb_repo}/distrib/",
        user    => "$mb_user",
        minute  => '15',
        hour    => '*/4',
    }

    # distrib/cauldron
    cron { 'MirrorBrain: Sync Cauldron every 1 hours ':
        command => "/usr/bin/null-rsync rsync.mageia.org::mageia/distrib/cauldron ${mb_repo}/distrib/",
        user    => "$mb_user",
        minute  => '0',
        hour    => '*/1',
    }

    # iso tree
    cron { 'MirrorBrain: Sync iso tree every 1 day ':
        command => "/usr/bin/null-rsync rsync.mageia.org::mageia/iso ${mb_repo}/",
        user    => "$mb_user",
        hour    => '2',
        minute  => '30',
    }

    # people tree
    cron { 'MirrorBrain: Sync people tree every 1 day ':
        command => "/usr/bin/null-rsync rsync.mageia.org::mageia/people ${mb_repo}/",
        user    => "$mb_user",
        hour    => '3',
        minute  => '45',
    }

    # software tree
    cron { 'MirrorBrain: Sync software tree every 1 day ':
        command => "/usr/bin/null-rsync rsync.mageia.org::mageia/software ${mb_repo}/",
        user    => "$mb_user",
        hour    => '4',
        minute  => '45',
    }

    # Mirror online check
    cron { 'MirrorBrain: mirror online status check every 5 minute':
        command => '/usr/bin/mirrorprobe',
        user    => "$mb_user",
        minute  => 5
    }

    # Mirror scanning
    cron { 'MirrorBrain: mirror scanning every 30 minute':
        command => '/usr/bin/mb scan --quiet --jobs 4 --all',
        user    => "$mb_user",
        minute  => 30
    }

    # Mirror database cleanup
    cron { 'MirrorBrain: mirror database cleanup every 1 week':
        command => '/usr/bin/mb db vacuum',
        user    => "$mb_user",
        minute  => 45,
        hour    => 5,
        weekday => 1
    }
}
