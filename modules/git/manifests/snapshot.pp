define git::snapshot( $source,
                      $refresh = '*/5',
                      $user = 'root',
                      $branch = 'master') {
    include git::client
    #TODO
    # should handle branch -> clone -n + branch + checkout
    # create a script
    # Idealy, should be handled by vcsrepo
    # https://github.com/bruce/puppet-vcsrepo
    # once it is merged in puppet
    exec { "/usr/bin/git clone -b ${branch} ${source} ${name}":
        creates => $name,
        user    => $user
    }

    if ($refresh != '0') {
        cron { "update ${name}":
            command => "cd ${name} && /usr/bin/git pull -q && /usr/bin/git submodule --quiet update --init --recursive",
            user    => $user,
            minute  => $refresh
        }
    }
}
