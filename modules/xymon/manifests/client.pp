class xymon::client {
    package { 'xymon-client': }

    if versioncmp($::lsbdistrelease, '5') < 0 {
        $service = 'xymon-client'
    } else {
        $service = 'xymon'
    }

    service { $service:
        hasstatus => false,
        status    => "${::lib_dir}/xymon/client/runclient.sh status",
        require   => Package['xymon-client'],
    }

    # TODO replace with a exported ressource
    $server = extlookup('hobbit_server','x')
    file { '/etc/sysconfig/xymon-client':
        content => template('xymon/xymon-client'),
        notify  => Service[$service],
        require => Package['xymon-client'],
    }
}
