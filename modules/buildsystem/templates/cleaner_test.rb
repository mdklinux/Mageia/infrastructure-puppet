require 'cleaner'
require "test/unit"
 
class TestCleaner < Test::Unit::TestCase

  @pkgs = []

  def setpackages(pkgs)
    @pkgs = pkgs
  end

  def packages(path)
    @pkgs.map{|p|
      l2 = p.split(':')
      sourcerpm = l2[0]
      filename = l2[1]
      buildtime = l2[2].to_i
      yield(sourcerpm, filename, buildtime)
    }
  end

  def test_old
    # Package was built on this arch and src.rpm for new version is 15d old
    setpackages(['foo-43-1.src.rpm:libfoo2-43-1.armv7hl.rpm:43', 'foo-42-1.src.rpm:libfoo1-42-1.armv7hl.rpm:42'])
    srcages = {}
    srcages['foo'] = [ 'foo-43-1.src.rpm', Time.now.to_i - 15*24*3600 ]
    srcs = {}
    srcs['foo-43-1.src.rpm'] = true
    assert_equal(['libfoo1-42-1.armv7hl.rpm'], check_binaries('armv7hl', srcs, srcages, '', '', nil))
  end

  def test_recent
    # Package was built on this arch but src.rpm for new version is only 1d old
    setpackages(['foo-43-1.src.rpm:foo-43-1.armv7hl.rpm:43', 'foo-42-1.src.rpm:foo-42-1.armv7hl.rpm:42'])
    srcages = {}
    srcages['foo'] = [ 'foo-43.src.rpm', Time.now.to_i - 24*3600 ]
    srcs = {}
    srcs['foo-43-1.src.rpm'] = true
    assert_equal([], check_binaries('armv7hl', srcs, srcages, '', '', nil))
  end

  def test_arm_late
    # Package was not yet built on this arch
    setpackages(['foo-42-1.src.rpm:foo-42-1.armv7hl.rpm:42'])
    srcages = {}
    srcages['foo'] = [ 'foo-43.src.rpm', Time.now.to_i - 24*3600 ]
    srcs = {}
    srcs['foo-43-1.src.rpm'] = true
    assert_equal([], check_binaries('armv7hl', srcs, srcages, '', '', nil))
  end
 
  def test_multiple_versions
    # Old package remains (usually happens to noarch due to youri bug)
    $noarch = { 'foo' => true }
    setpackages(['foo-42-1.src.rpm:foo-42-1.noarch.rpm:42', 'foo-42-2.src.rpm:foo-42-2.noarch.rpm:43'])
    srcages = {}
    srcages['foo'] = [ 'foo-42-2.src.rpm', Time.now.to_i - 24*3600 ]
    srcs = {}
    srcs['foo-42-2.src.rpm'] = true
    assert_equal(['foo-42-1.noarch.rpm'], check_binaries('i586', srcs, srcages, '', '', nil))
  end

  def test_icu
    $noarch = {}
    now = Time.now.to_i
    srctime = now - 3600
    oldbintime = now - 10*24*3600
    newbintime = now - 3200
    setpackages([
      "icu-71.1-2.mga9.src.rpm:icu71-data-71.1-2.mga9.noarch.rpm:#{oldbintime}",
      "icu-71.1-2.mga9.src.rpm:lib64icu71-71.1-2.mga9.aarch64.rpm:#{oldbintime}",
      "icu-72.1-1.mga9.src.rpm:icu72-data-72.1-1.mga9.noarch.rpm:#{newbintime}",
      "icu-72.1-1.mga9.src.rpm:lib64icu-devel-72.1-1.mga9.aarch64.rpm:#{newbintime}",
      "icu-72.1-1.mga9.src.rpm:lib64icu72-72.1-1.mga9.aarch64.rpm:#{newbintime}"
    ])
    srcages = {}
    srcages['icu'] = [ 'icu-71.1-2.mga9.src.rpm', srctime ]
    srcs = {}
    srcs['icu-71.1-2.mga9.src.rpm'] = true
    assert_equal([], check_binaries('aarch64', srcs, srcages, '', '', nil))
  end

end
