class buildsystem::release {
    git::snapshot { '/root/release':
        source => "git://git.${::domain}/software/infrastructure/release",
    }
}
