class buildsystem::iurt::user {
    include buildsystem::var::iurt

    buildsystem::sshuser { $buildsystem::var::iurt::login:
        homedir => $buildsystem::var::iurt::homedir,
    }

    file { '/etc/iurt':
        ensure => directory,
    }
}
