define buildsystem::iurt::config() {
    include buildsystem::var::iurt
    include buildsystem::var::webstatus
    include buildsystem::var::repository
    $distribution = $name
    # TODO rename the variable too in template
    $build_login = $buildsystem::var::iurt::login

    $build_timeout = {
      'default' => 36000,
      'atlas' => 57600,
      'chromium-browser-stable' => 172800,
      'clang' => 172800,
      'cross-gcc' => 115200,
      'gcc' => 115200,
      'itk' => 115200,
      'java-1.8.0-openjdk' => 172800,
      'kernel' => 115200,
      'libreoffice' => 432000,
      'llvm' => 57600,
      'openfoam' => 115200,
      'paraview' => 115200,
      'qgis' => 57600,
      'qtwebengine5' => 115200,
      'qtwebengine6' => 172800,
      'rust' => 180000,
      'salome' => 57600,
      'vtk' => 57600,
      'webkit' => 57600,
      'wrapitk' => 115200,
    }

    $allow_network_access = [
      'libguestfs', # Needs access to the configured mirrors
    ]

    file { "/etc/iurt/build/${distribution}.conf":
        owner   => $build_login,
        group   => $build_login,
        content => template("buildsystem/iurt.conf")
    }
}
