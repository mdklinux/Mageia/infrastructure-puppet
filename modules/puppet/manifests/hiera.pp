class puppet::hiera {
    package { ['ruby-hiera']: }

    if versioncmp($::lsbdistrelease, '4') < 0 {
        package { ['ruby-hiera-puppet']: }
        # ugly hack for puppet 2.7, since hiera has been integrated
        # from puppet 3 only (Mageia 4)
        file { '/etc/puppet/external/hiera':
            ensure  => link,
            # this should be /usr/share/ruby/gems/gems/hiera-puppet-1.0.0
            # on Mageia 3, but we do not have any infra hosts running mga3
            target  => '/usr/lib/ruby/gems/1.8/gems/hiera-puppet-0.3.0/',
            require => Package['ruby-hiera-puppet'],
        }
    }

    # ease the use fo the command line tool
    # who use a different location for the config file
    file { '/etc/hiera.yaml':
        ensure => link,
        target => '/etc/puppet/hiera.yaml',
    }

    file { '/etc/puppet/hiera.yaml':
        content => template('puppet/hiera.yaml'),
    }
}
