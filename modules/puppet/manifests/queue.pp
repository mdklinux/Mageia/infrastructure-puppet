class puppet::queue {
    include stompserver

    package { 'ruby-stomp': }

    service { 'puppetqd':
        provider => base,
        start    => 'puppet queue',
        require  => [Package['puppet-server'],
                    Package['ruby-stomp'],
                    File['/etc/puppet/puppet.conf']],
    }
}
