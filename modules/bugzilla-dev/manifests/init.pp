class bugzilla-dev {

    $bugzilla_dev_location = '/usr/share/bugzilla/'

    package {['graphviz',
              'perl-Template-GD', # needed for graphical_report support
              'perl-Test-Taint',
              'perl-JSON-RPC',
              'perl-Email-MIME',
              'perl-Email-Sender',
              'Math-Random-ISAAC',
              'perl-Chart',
              'perl-PatchReader',
              'perl-ldap',
              'perl-SOAP-Lite',
              'perl-XMLRPC-Lite',
              'perl-CGI']: }

    $pgsql_password = extlookup('bugzilla_pgsql','x')
    $ldap_password = extlookup('bugzilla_ldap','x')

    postgresql::remote_db_and_user { 'bugs':
        description => 'Bugzilla database',
        password    => $pgsql_password,
    }

    file { '/usr/share/bugzilla/localconfig':
      group   => 'apache',
      mode    => '0640',
      content => template('bugzilla-dev/localconfig')
    }


    file { '/usr/share/bugzilla/data/params.json':
      group   => 'apache',
      mode    => '0640',
      content => template('bugzilla-dev/params.json')
    }

    apache::webapp_other { 'bugzilla-dev':
        webapp_file => 'bugzilla-dev/webapp_bugzilla.conf',
    }

    $bugs_vhost = "bugs-dev.${::domain}"
    $vhost_root = '/usr/share/bugzilla'

    apache::vhost::redirect_ssl { $bugs_vhost: }

    apache::vhost::base { $bugs_vhost:
        content  => template('bugzilla-dev/vhost.conf'),
        aliases  => { '/bugzilla/' => $vhost_root },
        use_ssl  => true,
        location => $vhost_root,
        vhost    => $bugs_vhost,
    }

    git::snapshot { $bugzilla_dev_location:
      source  => "git://git.${::domain}/web/bugs"
    }

    file { 'Mageia':
      ensure  => directory,
      path    => '/usr/share/bugzilla',
      group   => 'apache',
      recurse => true,
      require => Git::Snapshot[$bugzilla_dev_location],
    }

    file { '/usr/share/bugzilla/robots.txt':
      group   => 'apache',
      mode    => '0640',
      content => template('bugzilla-dev/robots.txt')
    }

    cron { 'collectstats':
        command => '/usr/share/bugzilla/bin/collectstats.pl',
        user    => 'apache',
        hour    => 2,
        minute  => 30,
    }
}
