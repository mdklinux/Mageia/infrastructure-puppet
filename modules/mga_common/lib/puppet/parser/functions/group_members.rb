# group_members($group)
#   -> return a array with the login of the group members

module Puppet::Parser::Functions
    newfunction(:group_members, :type => :rvalue) do |args|
        group = args[0]
        `getent group`.each_line do |l|
	    if l =~ /^#{group}:/ then
                return l.chomp.split(':')[3].split(',')
	    end
        end
        raise ArgumentError, "can't find group for #{group}"
    end
end
