class openssh::server {
    # some trick to manage sftp server, who is arch dependent on mdv
    # TODO: the path changed on Mageia 6 to /usr/libexec/openssh/sftp-server
    $path_to_sftp = "${::lib_dir}/ssh/"

    package { 'openssh-server': }

    service { 'sshd':
        subscribe => Package['openssh-server'],
    }

    file { '/etc/ssh/sshd_config':
        require => Package['openssh-server'],
        content => template('openssh/sshd_config'),
        notify  => Service['sshd']
    }
}
