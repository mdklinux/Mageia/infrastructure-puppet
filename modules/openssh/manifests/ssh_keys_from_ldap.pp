class openssh::ssh_keys_from_ldap inherits server {
if versioncmp($::lsbdistrelease, '7') < 0 {
    package { 'python-ldap': }
} else {
    package { 'python2-ldap': }
}

    $ldap_pwfile = '/etc/ldap.secret'
    $nslcd_conf_file = '/etc/nslcd.conf'
    $ldap_servers = get_ldap_servers()
if versioncmp($::lsbdistrelease, '7') < 0 {
    mga_common::local_script { 'ldap-sshkey2file.py':
        content => template('openssh/ldap-sshkey2file.py'),
        require => Package['python-ldap']
    }
} else {
    mga_common::local_script { 'ldap-sshkey2file.py':
        content => template('openssh/ldap-sshkey2file.py'),
        require => Package['python2-ldap']
    }
}

    cron { 'sshkey2file':
        command     => '/bin/bash -c "/usr/local/bin/ldap-sshkey2file.py && ( [[ -f /usr/bin/mgagit && -d /var/lib/git/.gitolite ]] && /bin/su -c \'/usr/bin/mgagit glrun\' - git ) ||:"',
        hour        => '*',
        minute      => '*/10',
        user        => 'root',
        environment => 'MAILTO=root',
        require     => Mga_common::Local_script['ldap-sshkey2file.py'],
    }
}
